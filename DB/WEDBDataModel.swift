//
//  WEDBDataModel.swift
//  WeatherExplorer
//
//  Created by Ying Wai Shum on 23/6/2019.
//  Copyright © 2019年 Jimmy Shum. All rights reserved.
//

import Foundation
import RealmSwift

class WEDBDataEntry: Object{
    @objc dynamic var cityName: String = ""
    
    override class func primaryKey() -> String?{
        return "cityName"
    }
}
