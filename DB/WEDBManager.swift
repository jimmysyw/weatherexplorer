//
//  WEDBManager.swift
//  WeatherExplorer
//
//  Created by Ying Wai Shum on 23/6/2019.
//  Copyright © 2019年 Jimmy Shum. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class WEDBManager{
    private var  database:Realm
    static let   shared = WEDBManager()
    private init() {
        let config = Realm.Configuration(
            // Set the new schema version. This must be greater than the previously used
            // version (if you've never set a schema version before, the version is 0).
            schemaVersion: 0,
            
            // Set the block which will be called automatically when opening a Realm with
            // a schema version lower than the one set above
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        
        // Tell Realm to use this new configuration object for the default Realm
        Realm.Configuration.defaultConfiguration = config
        database = try! Realm()
    }
    func getDataFromDB() ->   Results<WEDBDataEntry> {
        let results: Results<WEDBDataEntry> =   database.objects(WEDBDataEntry.self)
        return results
    }
    func addData(object: WEDBDataEntry)   {
        try! database.write {
            database.add(object, update: true)
            print("Added new object")
        }
    }
    
    func deleteAllFromDatabase()  {
        try!   database.write {
            database.deleteAll()
        }
    }
    func deleteFromDb(object: WEDBDataEntry)   {
        try!   database.write {
            database.delete(object)
        }
    }
}

