//
//  WESearchViewModel.swift
//  WeatherExplorer
//
//  Created by Jimmy Shum on 20/6/2019.
//  Copyright © 2019 Jimmy Shum. All rights reserved.
//

import Foundation
import CoreLocation

struct WESearchEntry{
    var cityName: String = ""
}

class WESearchViewModel{

 
    var searchEntries: [WESearchEntry] = []
    
    var searchDidSuccess: (() -> Void)?
    var searchDidError: (() -> Void)?
    var searchDidFailure: (() -> Void)?
    
   init(){
        updateSearchEntries()
        setupNotificationObserver()
    }
    

    
    func proceedSearch(scopeTitle: String, searchText: String){
        print("scopeTitle: \(scopeTitle) searchText: \(searchText)")
        switch scopeTitle{
            case "City Name":
                let cityName = searchText
                WEDataStore.shared.requestWEData(service: WENetworkService.weatherByCityName(name: cityName))
            case "Zip Code":
                let searchTextArr = searchText.components(separatedBy: ",")
                if searchTextArr.count > 1{
                    let zipCode = String(searchTextArr[0])
                    let countryCode = String(searchTextArr[1])
                    WEDataStore.shared.requestWEData(service: WENetworkService.weatherByZipCode(code: zipCode, countryCode: countryCode))
                }
            case "Lat/Long":
                let searchTextArr = searchText.components(separatedBy: ",")
                if searchTextArr.count > 1{
                    if let lat = Double(searchTextArr[0]),
                        let long = Double(searchTextArr[1]){
                        WEDataStore.shared.requestWEData(service: WENetworkService.weatherByCoor(lat: lat, long: long))
                    }
                }
            case "GPS":
                NotificationCenter.default.addObserver(forName: LocationManagerNotification.locationRetrieveSuccess.name,
                                                       object: nil,
                                                       queue: OperationQueue.main,
                                                       using: { [weak self] (_) in
                                                        self?.processGPSSearch()
                })
                WELocationManager.shared.determineMyCurrentLocation()
            
            default: return
        }
    }
    func setupNotificationObserver() {
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveSuccess.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.searchDidSuccess?()
        })
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveError.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.searchDidError?()
        })
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveFail.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.searchDidFailure?()
        })
    }
    
    
    //MARK: SearchEntry methods
    func saveSearchEntry(searchEntry: WESearchEntry){
        let dbEntry = WEDBDataEntry()
        dbEntry.cityName = searchEntry.cityName
        WEDBManager.shared.addData(object: dbEntry)
        updateSearchEntries()
    }
    
    func deleteSearchEntry(searchEntry: WESearchEntry){
        WEDBManager.shared.deleteAllFromDatabase()
        for entry in searchEntries{
            if entry.cityName != searchEntry.cityName{
                saveSearchEntry(searchEntry: entry)
            }
        }
        updateSearchEntries()
    }
    func updateSearchEntries(){
        searchEntries = []
        for dbEntry in WEDBManager.shared.getDataFromDB(){
            let searchEntry = WESearchEntry(cityName: dbEntry.cityName)
            searchEntries.insert(searchEntry, at: 0)
        }
    }
    func processGPSSearch(){
        print("processGPSSearch")
        NotificationCenter.default.removeObserver(LocationManagerNotification.locationRetrieveSuccess.name)
        
        if let latLong = WELocationManager.shared.retrievedLatLong{
             WEDataStore.shared.requestWEData(service: WENetworkService.weatherByCoor(lat: latLong.0, long: latLong.1))
        }
      
    }
   
}
