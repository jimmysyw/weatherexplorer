//
//  WEMainViewModel.swift
//  WeatherExplorer
//
//  Created by Jimmy Shum on 20/6/2019.
//  Copyright © 2019 Jimmy Shum. All rights reserved.
//

import Foundation
import UIKit

enum DayStatus{
    case Day
    case Night
}
enum WeatherStatus{
    case Clear(DayStatus)
    case PartlyCloudy(DayStatus)
    case Cloudy(DayStatus)
    case ThunderStorm
    case Rain
    case Snow
    case Special(String)
    
    
    var image: UIImage?{
        switch self{
        case .Clear(let dayStatus):
            switch dayStatus{
            case .Day:      return UIImage(named: "clear_day")
            case .Night:    return UIImage(named: "clear_night")
            }
        case .PartlyCloudy(let dayStatus):
            switch dayStatus{
            case .Day:      return UIImage(named: "partly_cloudy_day")
            case .Night:    return UIImage(named: "partly_cloudy_night")
            }
        case .Cloudy(_):    return UIImage(named: "cloud")
        case .Rain:         return UIImage(named: "rain")
        case .ThunderStorm: return UIImage(named: "thunderstorm")
        case .Snow:         return UIImage(named: "snow")
        default:            return nil
        }
    }
    var bgImage: UIImage?{
        switch self{
        case .Clear(let dayStatus):
            switch dayStatus{
            case .Day:      return UIImage(named: "clear_day_bg")
            case .Night:    return UIImage(named: "clear_night_bg")
            }
        case .PartlyCloudy(let dayStatus):
            switch dayStatus{
            case .Day:      return UIImage(named: "partly_cloudy_bg")
            case .Night:    return UIImage(named: "cloudy_bg")
            }
        case .Cloudy(let dayStatus):
            switch dayStatus{
            case .Day:      return UIImage(named: "cloudy_day_bg")
            case .Night:    return UIImage(named: "cloudy_night_bg")
            }
        case .Rain:         return UIImage(named: "rain_bg")
        case .ThunderStorm: return UIImage(named: "thunderstorm_bg")
        case .Snow:         return UIImage(named: "snow_bg")
        default:            return nil
        }
    }
    var statusStr: String{
        switch self{
        case .Clear(let dayStatus):
            switch dayStatus{
            case .Day:      return "Sunny"
            case .Night:    return "Clear"
            }
        case .PartlyCloudy(_):  return "Partly Cloudy"
        case .Cloudy:           return "Cloudy"
        case .ThunderStorm:     return "Thunder Storm"
        case .Rain:             return "Rain"
        case .Snow:             return "Snow"
        case .Special(let str): return str
        }
    }
    
    static func getWeatherStatus(wsStr: String) -> WeatherStatus?{
        switch wsStr{
        case "Clouds":          return .Cloudy(.Day)
        case "Clear":           return .Clear(.Day)
        case "Rain":            return .Rain
        case "Thunderstorm":    return .ThunderStorm
        case "Snow":            return .Snow
        default:                return .Special(wsStr)
        }
    }
}

class WEMainViewEntry{
    var cityName: String?
    var weatherStatusStr: String?
    var weatherImage: UIImage?
    var weatherBGImage: UIImage?
    var weatherDesc: String?
    var currTemp: String?
    var minTemp: String?
    var maxTemp: String?
    var humidity: String?
    

    static func getMainViewEntry(dataEntry: WEDataEntry) -> WEMainViewEntry{
        let mainViewEntry = WEMainViewEntry()
        mainViewEntry.cityName = dataEntry.cityName
        
        if let weatherItems = dataEntry.weather{
            if weatherItems.count > 0{
                let weatherItem = weatherItems[0]
                if let wsStr = weatherItem.main{
                    let weatherStatus =  WeatherStatus.getWeatherStatus(wsStr: wsStr)
                    mainViewEntry.weatherStatusStr = weatherStatus?.statusStr
                    if let weatherImage = weatherStatus?.image{
                        mainViewEntry.weatherImage = weatherImage
                    }
                    if let weatherBGImage = weatherStatus?.bgImage{
                        mainViewEntry.weatherBGImage = weatherBGImage
                    }
                }
                mainViewEntry.weatherDesc = weatherItem.desc
            }
        }
        
        
        
        if let weatherMain = dataEntry.main{
            if let temp = weatherMain.temp{
                mainViewEntry.currTemp = getTempStr(temp: temp)
            }
            if let tempMax = weatherMain.tempMax{
                mainViewEntry.maxTemp = getTempStr(temp: tempMax)
            }
            if let tempMin = weatherMain.tempMin{
                mainViewEntry.minTemp = getTempStr(temp: tempMin)
            }
            if let humidity = weatherMain.humidity{
                mainViewEntry.humidity = "\(Int(humidity))"
            }
        }
        
        
        return mainViewEntry
    }
    
    static func getTempStr(temp: Double) -> String{
        let modifiedTemp = Int((temp - 273)*100)/100
        return "\(modifiedTemp)°C"
    }
    
}

class WEMainViewModel{
    var mainViewEntry: WEMainViewEntry?
    var mainViewEntryDidUpdated: (() -> Void)?
    var mainViewEntryUpdateFail: (() -> Void)?
    
    init(){
        setupViewModel()
    }
    func setupViewModel(){
        setupMainViewEntry()
    }
    func setupNotification(){
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveSuccess.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.updateMainViewEntry()
                                                
        })
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveError.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.mainViewEntryUpdateFail?()
                                                
        })
        NotificationCenter.default.addObserver(forName: DataStoreNotification.dataRetrieveFail.name,
                                               object: nil,
                                               queue: OperationQueue.main,
                                               using: { [weak self] (_) in
                                                self?.mainViewEntryUpdateFail?()
                                                
        })
    }
    func removeNotification(){
        NotificationCenter.default.removeObserver(DataStoreNotification.dataRetrieveSuccess.name)
        NotificationCenter.default.removeObserver(DataStoreNotification.dataRetrieveError.name)
        NotificationCenter.default.removeObserver(DataStoreNotification.dataRetrieveFail.name)
        NotificationCenter.default.removeObserver(LocationManagerNotification.locationRetrieveSuccess.name)
    }
    func setupMainViewEntry(){
        if WEDataStore.shared.weDataEntry == nil{
            setupNotification()
            
            
            //check most recent search entry first
            var searchEntry: WESearchEntry?
            for dbEntry in WEDBManager.shared.getDataFromDB(){
                searchEntry = WESearchEntry(cityName: dbEntry.cityName)
            }
            if let se = searchEntry{
                prepareWeatherDataByRecentSearch(searchEntry: se)
            }
                
            //if no recent search entry, get entry from current GPS location
            else{
                NotificationCenter.default.addObserver(forName: LocationManagerNotification.locationRetrieveSuccess.name,
                                                       object: nil,
                                                       queue: OperationQueue.main,
                                                       using: { [weak self] (_) in
                                                        self?.prepareWeatherDataByCurrLocation()
                })
                WELocationManager.shared.determineMyCurrentLocation()
            }
           
        }
        else{
            updateMainViewEntry()
        }
    }
    func updateMainViewEntry(){
        if let dataEntry = WEDataStore.shared.weDataEntry{
            mainViewEntry = WEMainViewEntry.getMainViewEntry(
                dataEntry: dataEntry)
            print("mainViewEntry: \(mainViewEntry)")
            mainViewEntryDidUpdated?()
        }
        removeNotification()
        

    }
    func prepareWeatherDataByRecentSearch(searchEntry: WESearchEntry){
        WEDataStore.shared.requestWEData(service: WENetworkService.weatherByCityName(name: searchEntry.cityName))
    }
    func prepareWeatherDataByCurrLocation(){
        if let latLong = WELocationManager.shared.retrievedLatLong{
            WEDataStore.shared.requestWEData(service: WENetworkService.weatherByCoor(lat: latLong.0, long: latLong.1))
        }
    }
}
