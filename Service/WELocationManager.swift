//
//  WELocationManager.swift
//  WeatherExplorer
//
//  Created by Jimmy Shum on 24/6/2019.
//  Copyright © 2019 Jimmy Shum. All rights reserved.
//

import Foundation
import CoreLocation

enum LocationManagerNotification: String, NotificationName {
    case locationRetrieveSuccess,locationRetrieveError
}
class WELocationManager: NSObject,CLLocationManagerDelegate{
    static let shared = WELocationManager()
    
    var locationManager: CLLocationManager?
    var retrievedLatLong: (Double,Double)?
    
    override init(){
        super.init()
        setupLocationManager()
    }
    
    //MARK: Location
    func setupLocationManager(){
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestAlwaysAuthorization()
    }
    
    func determineMyCurrentLocation() {
        if locationManager == nil{
            setupLocationManager()
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse {
            locationManager?.requestWhenInUseAuthorization()
        }else{
            locationManager?.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.authorizedAlways) {
            // The user accepted authorization
            locationManager?.startUpdatingLocation()
        } else if (status == CLAuthorizationStatus.authorizedWhenInUse){
            // The user accepted authorization
            locationManager?.startUpdatingLocation()
        }
        else{
            NotificationCenter.default.post(name: LocationManagerNotification.locationRetrieveError.name, object: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // Call stopUpdatingLocation() to stop listening for location updates,
        // other wise this function will be called every time when user location changes.
        // manager.stopUpdatingLocation()
        
        manager.stopUpdatingLocation()
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        let lat = userLocation.coordinate.latitude
        let long = userLocation.coordinate.longitude
        
       

        retrievedLatLong = (lat,long)
        NotificationCenter.default.post(name: LocationManagerNotification.locationRetrieveSuccess.name, object: nil)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
        NotificationCenter.default.post(name: LocationManagerNotification.locationRetrieveError.name, object: nil)
        
    }
}
