//
//  WENetworkService.swift
//  WeatherExplorer
//
//  Created by Jimmy Shum on 20/6/2019.
//  Copyright © 2019 Jimmy Shum. All rights reserved.
//

import Foundation
import Moya
import Alamofire

enum WENetworkService{
    case weatherByCityName(name: String)
    case weatherByZipCode(code: String, countryCode: String?)
    case weatherByCoor(lat: Double, long: Double)
}

class WENetworkServiceConstant{
    static let APPID = "95d190a434083879a6398aafd54d9e73"
}

extension WENetworkService: TargetType{
    var baseURL: URL{
        return URL(string: "http://api.openweathermap.org")!
    }
    
    var path: String{
        return "/data/2.5/weather"
    }
    
    var method: Moya.Method{
        return .get
    }
    
    var headers: [String : String]?{
        return nil
    }
    
    var sampleData: Data{
        return Data()
    }
    
    var task: Task{
        return .requestParameters(parameters: self.parameters, encoding: URLEncoding.queryString)
    }
    
    var parameters: [String: Any]{
        
        var params: [String: Any] = [:]
       
        params["appId"] = WENetworkServiceConstant.APPID
        
        switch self {
        case .weatherByCityName(let name):
            params["q"] = name
            break
        case .weatherByZipCode(let code, let countryCode):
            if let cCode = countryCode{
                params["zip"] = "\(code),\(cCode)"
            }
            else{
                params["zip"] = code
            }
            break
        case .weatherByCoor(let lat,let long):
            params["lat"] = lat
            params["lon"] = long
            break
        }
        
        
        
        return params
    }
    
}
