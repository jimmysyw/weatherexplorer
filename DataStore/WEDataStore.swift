//
//  WEDataStore.swift
//  WeatherExplorer
//
//  Created by Jimmy Shum on 21/6/2019.
//  Copyright © 2019 Jimmy Shum. All rights reserved.
//


import Foundation
import SwiftyJSON

enum DataStoreNotification: String, NotificationName {
    case dataRetrieveSuccess,dataRetrieveError,dataRetrieveFail
}

enum RequestInfo{
    case cityName(name: String)
    case cityZipCode(zipCode: String, countryCode: String)
    case cityCoor(lat: Double, long: Double)
}

class WEDataStore{
    static let shared = WEDataStore()
    
    var weDataEntry: WEDataEntry?
    

    
    func updateDataEntry(service: WENetworkService){
        weDataEntry = WEDataStore.getDataEntryFromDummyJSON()
//        requestWEData(service: service)
    }

    func requestWEData(service: WENetworkService){
        _ = WENetworkAdapter.request(
            target: service,
            success: {response in
                let json = JSON(response.data)
                if let itemDict = json.dictionaryObject{
                    if let code = itemDict["cod"] as? Int{
                        if code == 200{
                            if let weData = WEDataEntry(JSON: itemDict){
                                print("weData: \(weData)")
                                self.weDataEntry = weData
                                NotificationCenter.default.post(name: DataStoreNotification.dataRetrieveSuccess.name, object: nil)
                                return
                            }
                            
                        }
                    }
                   
                }
                NotificationCenter.default.post(name: DataStoreNotification.dataRetrieveError.name, object: nil)
                //                print("deliveryDataList: \(self.deliveryDataList)")
             
        },
            error: { response in
                NotificationCenter.default.post(name: DataStoreNotification.dataRetrieveError.name, object: nil)
        },
            failure: { response in
                NotificationCenter.default.post(name: DataStoreNotification.dataRetrieveFail.name, object: nil)
        })
    }
    
    static func getDataEntryFromDummyJSON() -> WEDataEntry?{
        if let path = Bundle.main.path(forResource: "we_sample", ofType: "json") {
            if let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe){
                if let json = try? JSON(data: data){
                    if let itemDict = json.dictionaryObject{
                        if let weData = WEDataEntry(JSON: itemDict){
                            return weData
                        }
                    }
                }
            }
        }
        return nil
    }

    
}
