//
//  WEDataModel.swift
//  WeatherExplorer
//
//  Created by Ying Wai Shum on 20/6/2019.
//  Copyright © 2019年 Jimmy Shum. All rights reserved.
//

import Foundation
import ObjectMapper

struct WEDataEntry: Mappable{
    var coord: WECoorDataEntry?
    var weather: [WEWeatherMainDataEntry]?
    var main: WEMainDataEntry?
    var wind: WEWindDataEntry?
    var visibility: Double?
    var cityName: String?
    init(){
        
    }
    init?(map: Map) {
    }
    mutating func mapping(map: Map) {
        coord         <- map["coord"]
        weather       <- map["weather"]
        main          <- map["main"]
        wind          <- map["wind"]
        visibility    <- map["visibility"]
        cityName      <- map["name"]
    }
}


struct WECoorDataEntry: Mappable{
    var lon: Double?
    var lat: Double?
    init(){
        
    }
    init?(map: Map) {
    }
    mutating func mapping(map: Map) {
        lon           <- map["lon"]
        lat           <- map["lat"]
    }
    
}
struct WEMainDataEntry: Mappable{
    var temp: Double?
    var pressure: Double?
    var humidity: Double?
    var tempMin: Double?
    var tempMax: Double?
    
    init(){
        
    }
    init?(map: Map) {
    }
    mutating func mapping(map: Map) {
        temp           <- map["temp"]
        pressure       <- map["pressure"]
        humidity       <- map["humidity"]
        tempMin        <- map["temp_min"]
        tempMax        <- map["temp_max"]
        
    }
}
struct WEWindDataEntry: Mappable{
    var speed:  Double?
    var deg:    Double?
    init(){
        
    }
    init?(map: Map) {
    }
    mutating func mapping(map: Map) {
        speed          <- map["speed"]
        deg            <- map["deg"]
    }
}

struct WEWeatherMainDataEntry: Mappable{
    var main: String?
    var desc: String?
    init(){
        
    }
    init?(map: Map) {
    }
    mutating func mapping(map: Map) {
        main           <- map["main"]
        desc           <- map["description"]
    }
}
struct WESysDataEntry: Mappable{
    var country: String?
    init(){
        
    }
    init?(map: Map) {
    }
    mutating func mapping(map: Map) {
        country         <- map["country"]
    }
    
}






