//
//  WEMainViewController.swift
//  WeatherExplorer
//
//  Created by Jimmy Shum on 20/6/2019.
//  Copyright © 2019 Jimmy Shum. All rights reserved.
//

import UIKit

class WEMainViewController: UIViewController {
    var weMainViewModel: WEMainViewModel?
    var preloadingCoverView: UIView?
    
    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var countryNameLbl: UILabel!
    @IBOutlet weak var weatherStatusImageView: UIImageView!
    @IBOutlet weak var weatherStatusLbl: UILabel!
    @IBOutlet weak var weatherStatusDescLbl: UILabel!
    @IBOutlet weak var weatherStatusBGImgView: UIImageView!
    
    @IBOutlet weak var currTempLbl: UILabel!
    @IBOutlet weak var maxTempHeaderLbl: UILabel!
    @IBOutlet weak var maxTempLbl: UILabel!
    
    @IBOutlet weak var minTempHeaderLbl: UILabel!
    @IBOutlet weak var minTempLbl: UILabel!
    
    @IBOutlet weak var humidityHeaderLbl: UILabel!
    @IBOutlet weak var humidityLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preloadingCoverView = UIView(frame: self.view.frame)
        preloadingCoverView?.backgroundColor = UIColor.white
        if let coverView = preloadingCoverView{
            self.view.addSubview(coverView)
        }
        setupViewModel()
        setupView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        weMainViewModel?.updateMainViewEntry()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setupViewModel(){
        
        weMainViewModel = WEMainViewModel()
        weMainViewModel?.mainViewEntryDidUpdated = {
            self.updateContent()
        }
        weMainViewModel?.mainViewEntryUpdateFail = {
            let alert = UIAlertController(title: "Weather Data loading Error", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    func setupView(){
        setupNavBar()
//        updateContent()
        
    }
    func setupNavBar(){
        //Search Button
        let searchButton = UIBarButtonItem.init(barButtonSystemItem: .search, target: self, action: #selector(searchButtonDidClicked))
        self.navigationItem.rightBarButtonItem = searchButton
    }
    func updateContent(){
        preloadingCoverView?.isHidden = true
        maxTempHeaderLbl.text = "Highest Temp."
        minTempHeaderLbl.text = "Lowest Temp."
        humidityHeaderLbl.text = "Himidity"
        
        if let mainVE = weMainViewModel?.mainViewEntry{
            cityNameLbl.text = mainVE.cityName
            countryNameLbl.text = ""
            weatherStatusLbl.text = mainVE.weatherStatusStr
            if let weatherImage = mainVE.weatherImage{
                weatherStatusImageView.isHidden = false
                weatherStatusImageView.image = weatherImage
            }
            else{
                weatherStatusImageView.isHidden = true
            }
            weatherStatusBGImgView.image = mainVE.weatherBGImage
            weatherStatusDescLbl.text = mainVE.weatherDesc
            currTempLbl.text = mainVE.currTemp
            maxTempLbl.text = mainVE.maxTemp
            minTempLbl.text = mainVE.minTemp
            humidityLbl.text = mainVE.humidity
        }
 
    }
    
    
    
    
    
    @objc func searchButtonDidClicked(){
        performSegue(withIdentifier: "SearchVCSegue", sender: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
