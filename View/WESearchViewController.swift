//
//  WESearchViewController.swift
//  WeatherExplorer
//
//  Created by Jimmy Shum on 20/6/2019.
//  Copyright © 2019 Jimmy Shum. All rights reserved.
//

import UIKit
import JGProgressHUD

class WESearchViewController: UITableViewController,UISearchResultsUpdating,UISearchBarDelegate{

    let searchController = UISearchController(searchResultsController: nil)
    var selectedScopeIndex: Int = 0
    var scopeTitleList: [String] = ["City Name", "Zip Code", "Lat/Long", "GPS"]
    var searchViewModel: WESearchViewModel?
    var progressView: JGProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        setupView()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    func setupViewModel(){
        searchViewModel = WESearchViewModel()
        searchViewModel?.searchDidSuccess = {
            if let cityName =  WEDataStore.shared.weDataEntry?.cityName{
                let searchEntry = WESearchEntry(cityName: cityName)
                self.searchViewModel?.saveSearchEntry(searchEntry: searchEntry)
            }
            self.hideLoadingView()
            self.navigationController?.popViewController(animated: true)
        }
        searchViewModel?.searchDidError = {
            self.hideLoadingView()
            let alert = UIAlertController(title: "Search Error", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
            print("search error")
        }
        searchViewModel?.searchDidFailure = {
            self.hideLoadingView()
            let alert = UIAlertController(title: "Search Failure", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true)
            print("search failure")
        }
    }
    func setupView(){
        setupLoadingView()
        setupSearchController()
    }
    func setupSearchController(){
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Enter City Name, Zip Code or Lat/Long"
//        searchController.searchBar.showsScopeBar = true
        searchController.searchBar.scopeButtonTitles = scopeTitleList
        searchController.searchBar.delegate = self
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
        
    }
    func updateSearchResults(for searchController: UISearchController) {
        print("updateSearchResults")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Recent Search"
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let entries = searchViewModel?.searchEntries{
            return entries.count
        }
        return 0
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard let entries = searchViewModel?.searchEntries else { return }
        if editingStyle == .delete {
            // Delete the row from the data source
            let entry = entries[indexPath.row]
            searchViewModel?.deleteSearchEntry(searchEntry: entry)
            tableView.reloadData()
//            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let entries = searchViewModel?.searchEntries else { return UITableViewCell() }
        
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "weSearchViewCell")

        cell.textLabel?.text = entries[indexPath.row].cityName
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let entries = searchViewModel?.searchEntries else { return }
        let selectedEntry = entries[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        
        if selectedEntry.cityName != ""{
            searchViewModel?.proceedSearch(scopeTitle: "City Name", searchText: selectedEntry.cityName)
        }

    }
    
    
    //MARK: Search bar delegate
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("search button clicked")
        startLoadingView()
        let scopeTitle = scopeTitleList[selectedScopeIndex]
        if let searchText = searchBar.text{
            searchViewModel?.proceedSearch(scopeTitle: scopeTitle, searchText: searchText)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("New scope index is now \(selectedScope)")
        selectedScopeIndex = selectedScope
        if selectedScopeIndex == 3{ //GPS
            startLoadingView()
            searchViewModel?.proceedSearch(scopeTitle: "GPS", searchText: "")
        }
    }
    
    //MARK: Loading View
    func setupLoadingView(){
        progressView = JGProgressHUD(style: .dark)
        progressView?.textLabel.text = "Loading"
        
    }
    
    func startLoadingView(){
        if let navView = navigationController?.view{
            progressView?.show(in: navView)
        }
    }
    func hideLoadingView(){
        progressView?.dismiss()
    }


}


