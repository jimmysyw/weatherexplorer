//
//  WETest.swift
//  WeatherExplorer
//
//  Created by Jimmy Shum on 21/6/2019.
//  Copyright © 2019 Jimmy Shum. All rights reserved.
//

import Foundation
import SwiftyJSON

class WETest{
    static func test(){
//        testService()
        testDataModel()
    }
    //MARK: Data Model test
    static func testDataModel(){
        if let path = Bundle.main.path(forResource: "we_sample", ofType: "json") {
            if let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe){
                if let json = try? JSON(data: data){
                    if let itemDict = json.dictionaryObject{
                        if let weData = WEDataEntry(JSON: itemDict){
                            print("weData: \(weData)")
                        }
                    }
                }
            }
        }
    }
  
    //MARK: Service test
    static func testService(){
        testCityNameService()
        testCityZipCodeService()
        testCityCoorService()
    }
    
    static func testCityNameService(){
        WEDataStore.shared.requestWEData(service: WENetworkService.weatherByCityName(name: "London"))
    }
    static func testCityZipCodeService(){
        WEDataStore.shared.requestWEData(service: WENetworkService.weatherByZipCode(code: "000000", countryCode: nil))
    }
    static func testCityCoorService(){
        WEDataStore.shared.requestWEData(service: WENetworkService.weatherByCoor(lat: 22.3193,long: 114.1694))
    }
}
